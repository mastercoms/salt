server {
    listen 80;

    server_name kotlindiscord.com;

    location / {
        return 301 https://$server_name$request_uri;
    }

    location ~* /\.well-known/acme-challenge/.* {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl http2;
    server_name kotlindiscord.com;

    ssl_certificate /etc/letsencrypt/live/kotlindiscord.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/kotlindiscord.com/privkey.pem;

    client_max_body_size 2000M;

    location / {
        proxy_pass http://localhost:8105/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    location ~* /\.well-known/acme-challenge/.* {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}
