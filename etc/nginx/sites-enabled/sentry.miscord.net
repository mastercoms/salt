server {
    listen 80;

    server_name sentry.miscord.net;

    location / {
        return 301 https://$server_name$request_uri;
    }

    location ~* /\.well-known/acme-challenge/.* {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl http2;
    server_name sentry.miscord.net;

    ssl_certificate /etc/letsencrypt/live/sentry.miscord.net/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/sentry.miscord.net/privkey.pem;

    client_max_body_size 100M;

    location / {
        proxy_pass http://localhost:9400/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
