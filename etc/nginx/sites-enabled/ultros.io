server {
    listen 80;

    server_name ultros.io;

    location / {
        return 301 https://$server_name$request_uri;
    }

    location ~* /\.well-known/acme-challenge/.* {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}

server {
    listen 443 ssl http2;
    server_name ultros.io;

    ssl_certificate /etc/letsencrypt/live/ultros.io/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/ultros.io/privkey.pem;

    client_max_body_size 16M;

    location / {
        proxy_pass http://localhost:8083/;
    }

    location ~* /\.well-known/acme-challenge/.* {
        allow all;
        root /srv/acme;
        try_files $uri =404;
    }
}