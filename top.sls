base:
  "*":
    - setup

    - init/keys
    - init/locales

    - init/docker
    - init/iptables
    - init/haveged
    - init/nginx/all
    - init/sshd
    - init/sudo

    - docker/datadog

    - iptables/save  # So we have the tables again when the system restarts. Arch always reloads them there.

  "shimmer.gserv.me":
    - init/nginx/shimmer

    # My stuff
#    - docker/discord-irc
    - docker/ghost
    - docker/portainer
    - docker/quassel-gdude
    - docker/syncthing
    - docker/ultros

    # Other services
    - docker/bjorn-sentry
    - docker/dumbo
    - docker/fabricom-bot
    - docker/glowstone-forum
    - docker/glowstone-maven
    - docker/glowstone-site
    - docker/journeymap-mappy
    - docker/journeymap-maven
#    - docker/journeymap-sentry
    - docker/kotdis-bot
    - docker/kotdis-maven
    - docker/kotdis-modmail
    - docker/kotdis-nextcloud
    - docker/kotdis-site
#    - docker/minecraft-bub
#    - docker/minecraft-mila
#    - docker/minecraft-aki
#    - docker/minecraft-pydis
    - docker/quassel-ayth
    - docker/raidbot

    - iptables/save  # So we have the tables again when the system restarts. Arch always reloads them there.
