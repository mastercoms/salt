init_docker:
  pkg.installed:
    - pkgs:
      - docker
      - docker-compose

  service.running:
    - name: docker
    - enable: true

    - watch:
      - file: init_docker_config

    - require:
      - file: init_docker_config
      - file: init_docker_compose
         
init_docker_config:
  file.managed:
    - name: /etc/docker/daemon.json
    - source: salt://etc/docker/daemon.json
    - template: jinja

init_docker_compose:
  file.recurse:
    - name: /srv/compose
    - source: salt://compose/
