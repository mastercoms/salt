init_keys:
  file.managed:
    - name: /home/gdude/.ssh/authorized_keys
    - source: salt://etc/authorized_keys
