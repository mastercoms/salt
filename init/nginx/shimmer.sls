init_nginx:
  pkg.installed:
    - pkgs:
      - nginx

  service.running:
    - name: nginx

    - enable: True
    - reload: True

    - watch:
      - file: init_nginx_files

      - acme: archivesmc.com

      - acme: gserv.me
      - acme: api.gserv.me

      - acme: amp.gserv.me
      - acme: aki.amp.gserv.me
      - acme: pydis.amp.gserv.me

      - acme: portainer.gserv.me
      - acme: sync.gserv.me

      - acme: kenmarecraftcentre.com

      - acme: kotlindiscord.com
      - acme: docker.kotlindiscord.com
      - acme: maven.kotlindiscord.com
      - acme: modmail.kotlindiscord.com
      - acme: nextcloud.kotlindiscord.com

      - acme: jm.gserv.me
      - acme: sentry.jm.gserv.me

      - acme: glowstone.net
      - acme: forums.glowstone.net
      - acme: repo.glowstone.net

      - acme: sentry.miscord.net

      - acme: mystic.services

      - acme: ultros.io
      - acme: bot.ultros.io

init_nginx_files:
  file.recurse:
    - name: /etc/nginx/sites-enabled
    - source: salt://etc/nginx/sites-enabled/

# SSL certs

archivesmc.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: archivesmc.com
    - aliases:
      - www.archivesmc.com

gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: gserv.me

portainer.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: portainer.gserv.me

api.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: api.gserv.me

amp.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: amp.gserv.me

aki.amp.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: aki.amp.gserv.me

pydis.amp.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: pydis.amp.gserv.me

sync.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: sync.gserv.me

kenmarecraftcentre.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: kenmarecraftcentre.com
    - aliases:
      - www.kenmarecraftcentre.com


kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: kotlindiscord.com


docker.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: docker.kotlindiscord.com

maven.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: maven.kotlindiscord.com

modmail.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: modmail.kotlindiscord.com

nextcloud.kotlindiscord.com:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: nextcloud.kotlindiscord.com


jm.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: jm.gserv.me

sentry.jm.gserv.me:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: sentry.jm.gserv.me


glowstone.net:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: glowstone.net
    - aliases:
      - www.glowstone.net

forums.glowstone.net:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: forums.glowstone.net

repo.glowstone.net:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: repo.glowstone.net


mystic.services:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: mystic.services
    - aliases:
      - www.mystic.services


sentry.miscord.net:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: sentry.miscord.net


ultros.io:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: ultros.io
    - aliases:
      - www.ultros.io

bot.ultros.io:
  acme.cert:
    - email: gareth@gserv.me
    - webroot: /srv/acme
    - renew: 14
    - certname: bot.ultros.io
