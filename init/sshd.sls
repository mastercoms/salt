init_sshd:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 23
    - jump: ACCEPT

  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://etc/ssh/sshd_config

  service.running:
    - name: sshd
    - enable: True
