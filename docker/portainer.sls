portainer-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/portainer

portainer:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/portainer
