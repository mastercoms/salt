datadog-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/datadog

datadog:
  environ.setenv:
    - name: DD_API_KEY
    - value: {{ pillar["env"]["DD_API_KEY"] }}

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/datadog
