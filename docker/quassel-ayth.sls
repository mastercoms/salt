quassel_ayth-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/quassel-ayth

quassel_ayth:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/quassel-ayth

iptables_quassel_ayth:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 4242
    - jump: ACCEPT
