ghost-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/ghost

ghost:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/ghost
