kotdis-bot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/kotdis-bot

kotdis-bot:
  environ.setenv:
    - value:
        BOT_TOKEN: "{{ pillar["env"]["KOTDIS_BOT_TOKEN"] }}"
        BOT_APIKEY: "{{ pillar["env"]["KOTDIS_API_KEY"] }}"
        SENTRY_DSN: "{{ pillar["env"]["KOTDIS_BOT_DSN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/kotdis-bot
