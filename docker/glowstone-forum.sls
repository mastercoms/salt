glowstone-forum-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/glowstone-forum

glowstone-forum:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/glowstone-forum
