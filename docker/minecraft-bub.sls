minecraft-bub-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/minecraft-bub

iptables_minecraft_bub_tcp:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 25565
    - jump: ACCEPT

iptables_minecraft_bub_udp:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 25565
    - jump: ACCEPT

minecraft-bub:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/minecraft-bub
