journeymap-sentry-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/journeymap-sentry

journeymap-sentry:
  environ.setenv:
    - value:
        POSTGRES_USER: "{{ pillar["env"]["JOURNEYMAP_SENTRY_POSTGRES_USER"] }}"
        POSTGRES_PASSWORD: "{{ pillar["env"]["JOURNEYMAP_SENTRY_POSTGRES_PASSWORD"] }}"

        SENTRY_DB_PASSWORD: "{{ pillar["env"]["JOURNEYMAP_SENTRY_POSTGRES_PASSWORD"] }}"
        SENTRY_DB_USER: "{{ pillar["env"]["JOURNEYMAP_SENTRY_POSTGRES_USER"] }}"

        SENTRY_EMAIL_HOST: "{{ pillar["env"]["JOURNEYMAP_SENTRY_EMAIL_HOST"] }}"
        SENTRY_EMAIL_PORT: "{{ pillar["env"]["JOURNEYMAP_SENTRY_EMAIL_PORT"] }}"
        SENTRY_EMAIL_USER: "{{ pillar["env"]["JOURNEYMAP_SENTRY_EMAIL_USER"] }}"
        SENTRY_EMAIL_PASSWORD: "{{ pillar["env"]["JOURNEYMAP_SENTRY_EMAIL_PASSWORD"] }}"
        SENTRY_EMAIL_USE_TLS: "{{ pillar["env"]["JOURNEYMAP_SENTRY_EMAIL_USE_TLS"] }}"

        SENTRY_SECRET_KEY: "{{ pillar["env"]["JOURNEYMAP_SENTRY_SECRET_KEY"] }}"
        SENTRY_SERVER_EMAIL: "{{ pillar["env"]["JOURNEYMAP_SENTRY_SERVER_EMAIL"] }}"


  module.run:
    - name: dockercompose.up
    - path: /srv/compose/journeymap-sentry
