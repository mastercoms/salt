minecraft-mila-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/minecraft-mila

iptables_minecraft_mila_tcp:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 25566
    - jump: ACCEPT

iptables_minecraft_mila_udp:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 25566
    - jump: ACCEPT

minecraft-mila:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/minecraft-mila
