relaybot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/relaybot

relaybot:
  environ.setenv:
    - value:
        LOG_CHANNEL: "{{ pillar["env"]["RELAYBOT_LOG_CHANNEL"] }}"
        OWNER_ID: "{{ pillar["env"]["RELAYBOT_OWNER_ID"] }}"
        TOKEN: "{{ pillar["env"]["RELAYBOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/relaybot
