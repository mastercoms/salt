miscord-fsj-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/miscord-fsj

miscord-fsj:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/miscord-fsj
