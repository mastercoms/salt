glowstone-site-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/glowstone-site

glowstone-site:
  environ.setenv:
    - value:
        ADMIN_USERNAME: "{{ pillar["env"]["GLOWSTONE_SITE_ADMIN_USERNAME"] }}"
        DATABASE_URL: "{{ pillar["env"]["GLOWSTONE_SITE_DB_URL"] }}"
        POSTGRES_PASSWORD: "{{ pillar["env"]["GLOWSTONE_SITE_DB_PASSWORD"] }}"
        RECAPTCHA_SECRET: "{{ pillar["env"]["GLOWSTONE_SITE_RECAPTCHA_SECRET"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/glowstone-site
