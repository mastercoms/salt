ultros-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/ultros

ultros:
  environ.setenv:
    - value:
        ADMIN_USERNAME: "{{ pillar["env"]["ULTROS_ADMIN_USERNAME"] }}"
        DATABASE_URL: "{{ pillar["env"]["ULTROS_DB_URL"] }}"
        POSTGRES_PASSWORD: "{{ pillar["env"]["ULTROS_DB_PASSWORD"] }}"
        RECAPTCHA_SECRET: "{{ pillar["env"]["ULTROS_RECAPTCHA_SECRET"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/ultros
