kotdis-maven-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/kotdis-maven

kotdis-maven:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/kotdis-maven
