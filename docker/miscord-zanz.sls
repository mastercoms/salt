miscord-zanz-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/miscord-zanz

miscord-zanz:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/miscord-zanz
