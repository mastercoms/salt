journeymap-mappy-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/journeymap-mappy

journeymap-mappy:
  environ.setenv:
    - value:
        BOT_TOKEN: "{{ pillar["env"]["JOURNEYMAP_BOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/journeymap-mappy
