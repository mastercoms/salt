discord-irc-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/discord-irc
discord-irc:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/discord-irc
