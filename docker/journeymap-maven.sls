journeymap-maven-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/journeymap-maven

journeymap-maven:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/journeymap-maven
