raidbot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/raidbot

raidbot:
  environ.setenv:
    - value:
        AWS_KEY_ID: "{{ pillar["env"]["RAIDBOT_AWS_KEY_ID"] }}"
        AWS_KEY_SECRET: "{{ pillar["env"]["RAIDBOT_AWS_KEY_SECRET"] }}"
        CLIENT_ID: "{{ pillar["env"]["RAIDBOT_CLIENT_ID"] }}"
        CLIENT_SECRET: "{{ pillar["env"]["RAIDBOT_CLIENT_SECRET"] }}"
        SESSION_SECRET: "{{ pillar["env"]["RAIDBOT_SESSION_SECRET"] }}"
        TOKEN: "{{ pillar["env"]["RAIDBOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/raidbot
