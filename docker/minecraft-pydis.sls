minecraft-pydis-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/minecraft-pydis

iptables_minecraft_pydis_tcp:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 25567
    - jump: ACCEPT

iptables_minecraft_pydis_udp:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 25567
    - jump: ACCEPT

minecraft-pydis:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/minecraft-pydis
