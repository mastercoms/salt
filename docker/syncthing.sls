syncthing-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/syncthing

syncthing:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/syncthing

iptables_syncthing_listening:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 22000
    - jump: ACCEPT

iptables_syncthing_discovery:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 21027
    - jump: ACCEPT
