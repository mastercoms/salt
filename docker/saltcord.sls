saltcord-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/saltcord

saltcord:
  environ.setenv:
    - value:
        BOT_TOKEN: "{{ pillar["env"]["SALTCORD_TOKEN"] }}"
        SALT_PASSWORD: "{{ pillar["env"]["SALTCORD_SALT_PASSWORD"] }}"
        SALT_URL: "{{ pillar["env"]["SALTCORD_SALT_URL"] }}"
        SALT_USERNAME: "{{ pillar["env"]["SALTCORD_SALT_USERNAME"] }}"
        UPDATES_CHANNEL: "{{ pillar["env"]["SALTCORD_UPDATES_CHANNEL"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/saltcord
