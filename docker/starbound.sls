starbound-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/starbound

starbound:
  environ.setenv:
    - value:
        STEAM_USERNAME: "{{ pillar["env"]["STEAM_USERNAME"] }}"
        STEAM_PASSWORD: "{{ pillar["env"]["STEAM_PASSWORD"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/starbound

iptables_starbound:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 21025
    - jump: ACCEPT
