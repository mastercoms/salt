kotdis-nextcloud-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/kotdis-nextcloud

kotdis-nextcloud:
  environ.setenv:
    - value:
        NEXTCLOUD_ADMIN_USER: "{{ pillar["env"]["KOTDIS_NEXTCLOUD_ADMIN_USER"] }}"
        NEXTCLOUD_ADMIN_PASSWORD: "{{ pillar["env"]["KOTDIS_NEXTCLOUD_ADMIN_PASSWORD"] }}"
        POSTGRES_PASSWORD: "{{ pillar["env"]["KOTDIS_NEXTCLOUD_POSTGRES_PASSWORD"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/kotdis-nextcloud
