minecraft-aki-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/minecraft-aki

iptables_minecraft_aki_tcp:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 25568
    - jump: ACCEPT

iptables_minecraft_aki_udp:
  iptables.append:
    - chain: INPUT
    - protocol: udp
    - dport: 25568
    - jump: ACCEPT

minecraft-aki:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/minecraft-aki
