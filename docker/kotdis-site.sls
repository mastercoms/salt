kotdis-site-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/kotdis-site

kotdis-site:
  environ.setenv:
    - value:
        API_KEY: "{{ pillar["env"]["KOTDIS_API_KEY"] }}"
        DB_PASSWORD: "{{ pillar["env"]["KOTDIS_SITE_DB_PASS"] }}"
        DISCORD_CLIENT_ID: "{{ pillar["env"]["KOTDIS_SITE_DISCORD_CLIENT_ID"] }}"
        DISCORD_CLIENT_SECRET: "{{ pillar["env"]["KOTDIS_SITE_DISCORD_CLIENT_SECRET"] }}"

        POSTGRES_PASSWORD: "{{ pillar["env"]["KOTDIS_SITE_DB_PASS"] }}"

        SENTRY_DSN: "{{ pillar["env"]["KOTDIS_SITE_DSN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/kotdis-site
