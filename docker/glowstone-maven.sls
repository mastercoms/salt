glowstone-maven-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/glowstone-maven

glowstone-maven:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/glowstone-maven
