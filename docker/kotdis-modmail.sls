kotdis-modmail-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/kotdis-modmail

kotdis-modmail:
  environ.setenv:
    - value:
        TOKEN: "{{ pillar["env"]["KOTDIS_MODMAIL_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/kotdis-modmail
