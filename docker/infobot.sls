infobot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/infobot

infobot:
  environ.setenv:
    - value:
        LOG_CHANNEL: "{{ pillar["env"]["INFOBOT_LOG_CHANNEL"] }}"
        OWNER_ID: "{{ pillar["env"]["INFOBOT_OWNER_ID"] }}"
        TOKEN: "{{ pillar["env"]["INFOBOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/infobot
