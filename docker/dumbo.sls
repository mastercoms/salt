dumbo-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/dumbo

dumbo:
  environ.setenv:
    - value:
        BOT_TOKEN: "{{ pillar["env"]["DUMBO_BOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/dumbo
