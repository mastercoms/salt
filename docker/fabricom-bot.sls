fabricom-bot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/fabricom-bot

fabricom-bot:
  environ.setenv:
    - value:
        BOT_TOKEN: "{{ pillar["env"]["FABRICOM_BOT_TOKEN"] }}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/fabricom-bot
