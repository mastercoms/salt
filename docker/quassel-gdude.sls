quassel_gdude-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/quassel-gdude

quassel_gdude:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/quassel-gdude

iptables_quassel_gdude:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 4243
    - jump: ACCEPT
