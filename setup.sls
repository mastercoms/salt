setup:
  pkg.installed:
    - pkgs:
      - base-devel
      - git
      - haveged
      - htop
      - mosh
      - net-tools
      - p7zip
      - python
      - python2
      - python-pip
      - python2-pip
      - python-dateutil
      - python2-dateutil
      - python-docker
      # No longer available in Arch's repos
      #- python2-docker
      - python-cherrypy
      - python2-cherrypy
      - screen  # For long-running tasks I may need to disconnect during
      - sudo

      - {{ pillar['editor'] }}  # Editor (eg, nano)

  pip.installed:
    - name: docker
    - name: docker-compose

  user.present:
    - name: gdude
    - home: /home/gdude

  timezone.system:
    - name: Europe/Dublin

update:
  pkg.uptodate:
    - name: "Update"
    - refresh: True
